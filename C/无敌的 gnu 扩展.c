这串代码用来获取用户的家，如果实在获取不到就用 `/etc`。
获取方式是：
+ $HOME 环境变量
+ `/etc/passwd` 中的项
+ `/home/$username`

但是由于用了无敌的 GNU C 扩展，所以导致代码可能不那么……

```c
char *get_default_watch_on(void)
{
	return getenv("HOME") ? :
		({ struct passwd *p; (p = getpwuid(getuid())) ? p->pw_dir : 
		 (({ char p[5 + _POSIX_LOGIN_NAME_MAX]; sprintf(p, "/home/%s", getlogin()); strdup(p); }) ? :
		  (char *) (fprintf(stderr, "Warning: using /etc as watch-on directory"), "/etc") );});
}
```